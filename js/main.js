(function () {
	var tags = 'london',
		script = document.createElement('script'), actions, setup, handlers, selectedImages = [], activeClass = 'selected', photos;

	script.src = 'http://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=cb&tags='+tags;
	document.head.appendChild(script);
    //
	////original function declaration 'function cb(data){}' didnt work, the function wasnt attached (hoisted) to the same scope (window) as the the callback from the flicker api
	////it could have been fixed bu moving the same function declaration outside the scope of this self executing function, or alternativly,
	//// i have created an anonymous function and assigned it to a variable, which is actually attached to the window. allowing the callback to access it.
	cb = function (data) {
		// use returned data
		photos = data.items;
		setup.init();
	};

	setup = {
		init: function () {
			if (JSON.parse(localStorage.getItem('selectedImages')) instanceof Array) {
				selectedImages = JSON.parse(localStorage.getItem('selectedImages'))
			}
			setup.createDom();
		},
		bindEvent: function (){},
		createDom: function () {
			for (var i = 0; i < photos.length; i++) {
				actions.addPhotoToList(photos[i])
			}
		}
	};

	handlers = {
		click: {
			imageSelected: function (e) {
				var imageElement = e.target;
				if(imageElement.classList.contains(activeClass)) {
					actions.deselectImage(imageElement);
				} else {
					actions.selectImage(imageElement)
				}
			}
		}
	};

	actions = {
		selectImage: function (element) {
			var imgId = element.getAttribute('data-id');
			element.classList.add(activeClass);

			if (!actions.isImagePreselected(element)) {
				selectedImages.push(imgId);
				actions.setLocalStorage();
			}
		},
		deselectImage: function (element) {
			var imgIndex;
			element.classList.remove(activeClass);

			imgIndex = selectedImages.indexOf(element.getAttribute('data-id'));

			//remove the selected image index from the selected images array
			if (imgIndex > -1) {
				selectedImages.splice(imgIndex, 1);
			}
			actions.setLocalStorage();
		},
		addPhotoToList: function (photo_src) {
			var img = document.createElement("img"),
				listItem = document.createElement("li");

			listItem.classList.add('flicker-feed__item');
			img.setAttribute('src', photo_src.media.m);
			img.setAttribute('data-id', photo_src.link);

			if (actions.isImagePreselected(img)) {
				actions.selectImage(img);
			}
			listItem.appendChild(img);

			document.getElementById('flickr-feed').appendChild(listItem);
			img.addEventListener('click', handlers.click.imageSelected);
		},
		isImagePreselected: function (img) {
			return (selectedImages.indexOf(img.getAttribute('data-id')) > -1)
		},
		setLocalStorage: function () {
		//remove element from local storage
		localStorage.setItem('selectedImages', JSON.stringify(selectedImages));
		}
	};

	return {
		init: setup.init
	}
})();