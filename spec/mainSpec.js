describe('Selecting images', function () {
    jasmine.getFixtures().fixturesPath = 'spec/fixtures';

    beforeEach(function() {
        //setFixtures('<ul id="flickr-feed" class="flickr-feed"></ul>');
        //var fixture = setFixtures('<ul id="flickr-feed" class="flickr-feed"></ul>');
        //preloadFixtures('flicker-feed.html');
        loadFixtures('flicker-feed.html');
        //spyOn(location, 'reload')
    });

    it('should append the flickr feed images to the dom', function () {
        expect($('#flickr-feed')).toContainHtml('li');
    });
    it('should toggle active class when image when is clicked', function () {
        var listItemImg = $('#flickr-feed').find('li:first img');
        listItemImg.trigger('click');
        expect(listItemImg).toHaveClass('selected');
    });
    it('should deselect and remove active a selected image by clicking on it', function () {
        var listItemImg = $('#flickr-feed').find('li:first img');
        listItemImg.trigger('click');
        expect(listItemImg).not.toHaveClass('selected');
    });
    xit('should remember selected image when the page is reloaded', function () {
        //document.getElementById().click();
        var listItemImg = $('#flickr-feed').find('li:first img');
        listItemImg.trigger('click');
        location.reload();
        expect(listItemImg).toHaveClass('selected');
    });
});